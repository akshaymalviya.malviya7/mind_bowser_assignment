import React from 'react';
import {View, TouchableOpacity, ScrollView, Text, Image, ImageBackground} from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import LOADING from '../../assets/loading.png';
import Style from './style'

const DetailScreenView = (props) => {

  const {navigation,isThisItemFavourite,loading,handleTextChange,onFavouritePress} = props;

    return (
       <ScrollView style={Style.mainContainer}>
         <View style={Style.imageContainer}>
           <ImageBackground
             resizeMode='contain'
             onLoadStart={()=>handleTextChange(true,'loading')}
             onLoadEnd={()=>handleTextChange(false,'loading')}
             //loadingIndicatorSource={LOADING}
             style={Style.imageBackground}
             source={{uri: navigation.getParam('image')}}>
             {
               loading&&
                 <View style={Style.loading}>
                   <Image source={LOADING} style={Style.loadingImage}/>
                 </View>
             }
             <TouchableOpacity
               onPress={onFavouritePress}
               style={Style.favourite}>
               {
                 isThisItemFavourite?
                   <MaterialIcons name={'favorite'} color={'#ff0000'} size={40}/>
                   :
                   <MaterialIcons name={'favorite-outline'} color={'#000'} size={40}/>
               }
             </TouchableOpacity>
           </ImageBackground>
         </View>
         <View style={Style.detailContainer}>
           <View style={Style.titleContainer}>
             <Text style={Style.titleText}>Title: </Text>
             <Text style={Style.title}>{navigation.getParam('title')===" "?'No Title':navigation.getParam('title')}</Text>
           </View>
           <View style={Style.descriptionContainer}>
             <Text style={Style.titleText}>Description: </Text>
             <Text style={Style.title}>{navigation.getParam('description')}</Text>
           </View>
           <Text style={Style.date}>Uploaded on: {navigation.getParam('uploadDate')}</Text>
         </View>
       </ScrollView>
    )
};

export default DetailScreenView;
