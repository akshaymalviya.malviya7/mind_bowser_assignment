import React, {useState,useEffect} from 'react';
import DetailScreenView from './view';
import connect from 'react-redux/lib/connect/connect';
import {favouriteItem} from '../../module/favouriteItem';

const DetailScreen = (props) => {

    const [state,setStates] = useState({
        data:[],
        search:'',
        pageNumber:0,
        totalItem:0,
        isThisItemFavourite:false,
        loading:false
    });

    const handleTextChange = (text,field) => {
        setStates(prev => ({
            ...prev,
            [field]: text,
        }))
    };

    useEffect(()=>{
        if (props.favouriteItemData.find(o => o.id === props.navigation.getParam('id'))!==undefined){
            handleTextChange(true,'isThisItemFavourite');
        }
    },[]);

    const onFavouritePress = () => {
        const {navigation} = props;
            if (state.isThisItemFavourite===false){
                if(props.favouriteItemData.length<5){
                    const payload = [{
                        image: navigation.getParam('image'),
                        title:navigation.getParam('title'),
                        id: navigation.getParam('id'),
                        description: navigation.getParam('description'),
                        uploadDate: navigation.getParam('uploadDate')
                    }];
                    props.dispatch(favouriteItem(payload.concat(props.favouriteItemData))).then(()=>{
                        handleTextChange(true,'isThisItemFavourite');
                    })
                }else{
                    console.log("5 hogaye");
                }
            }else{
                let removedArray = props.favouriteItemData;
                for (let i=0;i<props.favouriteItemData.length;i++){
                    if (props.favouriteItemData[i].id===props.navigation.getParam('id')){
                        removedArray.splice(i,1);
                    }
                }
                props.dispatch(favouriteItem(removedArray)).then(()=>{
                    handleTextChange(false,'isThisItemFavourite');
                    navigation.state.params.rerender();
                });
            }
    };

    return (
        <DetailScreenView
            navigation={props.navigation}
            onFavouritePress={onFavouritePress}
            handleTextChange={handleTextChange}
            isThisItemFavourite={state.isThisItemFavourite}
            loading={state.loading}
        />
    )
};

DetailScreen.navigationOptions = ({ navigation }) => ({
    title:'Detail Screen'
});

const mapStateToProps = state => ({
    favouriteItemData: state.favouriteItem.data,
});

export default connect(mapStateToProps)(DetailScreen);
