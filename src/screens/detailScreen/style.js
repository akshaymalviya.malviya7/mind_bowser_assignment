import {Dimensions, StyleSheet} from 'react-native';

const screenHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1
  },
  imageContainer:{
    width:'100%',
    backgroundColor:'#000',
    height:screenHeight*0.4
  },
  imageBackground:{
    justifyContent:'flex-end',
    alignItems:'flex-end',
    width:'100%',
    height:'100%'
  },
  loading:{
    position:'absolute',
    height:'100%',
    width:'100%',
    justifyContent:'center',
    alignItems:'center'
  },
  loadingImage:{
    resizeMode:'center',
    height:'25%'
  },
  favourite:{
    marginBottom:10,
    marginRight:10,
    justifyContent:'center',
    backgroundColor:'#fff',
    alignItems:'center',
    height:screenHeight*0.1,
    width:screenHeight*0.1,
    borderRadius:screenHeight*0.05
  },
  detailContainer:{
    alignItems:'center',
    padding:10,
    width:'100%'
  },
  titleContainer:{
    flexDirection:'row',
    width:'100%',
    borderBottomWidth:0.5,
    borderColor:'lightgrey',
    paddingBottom:15
  },
  titleText:{
    color:'#000',
    fontWeight:'bold',
    fontSize:20
  },
  title:{
    color:'#000',
    fontSize:20,
    flex:1
  },
  descriptionContainer:{
    flexDirection:'row',
    width:'100%',
    paddingTop:15
  },
  date:{
    color:'#000',
    fontSize:14,
    width:'100%',
    marginTop:15
  }
});

export default styles;
