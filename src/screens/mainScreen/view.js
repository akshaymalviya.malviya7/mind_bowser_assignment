import React from 'react';
import {View,Image,TouchableOpacity,Text,ImageBackground} from 'react-native';
import LOGO from '../../assets/logoMB.png';
import night from '../../assets/night.png';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Style from './style'

const MainScreenView = (props) => {

  const {navigation} = props;
    return (
       <ImageBackground style={Style.mainContainer} source={night}>
               <Image source={LOGO} style={Style.logo}/>
               <View style={Style.textContainer}>
                   <Text style={Style.text}>GIF Image</Text>
                   <Text style={Style.text}>Searcher</Text>
               </View>
               <TouchableOpacity
                 onPress={()=>navigation.navigate('StateScreen')}
                   style={Style.button}>
                   <Text style={Style.buttonText}>Start Searching</Text>
                 <MaterialIcons name={'navigate-next'} color={'#fff'} size={25}/>
               </TouchableOpacity>
       </ImageBackground>
    )
};

export default MainScreenView;
