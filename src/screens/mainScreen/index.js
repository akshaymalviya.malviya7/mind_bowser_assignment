import React from 'react';
import MainScreenView from './view';
import connect from 'react-redux/lib/connect/connect';

const MainScreen = (props) => {

    return (
        <MainScreenView
            navigation={props.navigation}
        />
    )
};

MainScreen.navigationOptions = ({ navigation }) => ({
    header:null
});

const mapStateToProps = state => ({
    fetching: state.fetchData.fetching,
});

export default connect(mapStateToProps)(MainScreen);
