import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  mainContainer: {
    alignItems:'center',
    flex:1,
    paddingTop:20
  },
  logo:{
    resizeMode:'contain',
    height:'10%'
  },
  textContainer:{
    flex:1,
    alignItems:'center',
    paddingTop:20
  },
  text:{
    fontSize:22,
    color:'#fff'
  },
  button:{
    elevation:4,
    paddingHorizontal:10,
    flexDirection:'row',
    marginBottom:20,
    justifyContent:'center',
    alignItems:'center',
    alignSelf:'center',
    height:45,
    backgroundColor:'rgba(201,109,22,0.85)',
    borderRadius:5
  },
  buttonText:{
    color:'#fff',
    fontWeight:'bold',
    fontSize:16
  }
});

export default styles;
