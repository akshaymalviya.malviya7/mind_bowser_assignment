import {Dimensions, StyleSheet} from 'react-native';

const screenWidth = Dimensions.get('window').width;

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1
  },
  flatList: {
    flex: 1
  },
  touchable:{
    width:screenWidth/3,
    padding:5
  },
  cardView:{
    width:'100%',
    backgroundColor:'#000',
    flexDirection:'row',
    height:screenWidth/3,
    borderWidth:0.5
  },
  image:{
    flex:1,
    justifyContent:'flex-end',
    alignItems:'flex-end'
  },
  favourite:{
    marginBottom:5,
    marginRight:5,
    justifyContent:'center',
    backgroundColor:'#fff',
    alignItems:'center',
    height:'25%',
    width:'25%',
    borderRadius:50
  },
  noItemContainer:{
    flex:1,
    justifyContent:'center',
    alignItems:'center'
  },
  noItemText:{
    fontWeight:'bold',
    color:'lightgrey',
    fontSize:22
  }
});

export default styles;
