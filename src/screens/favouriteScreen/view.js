import React from 'react';
import {View, TouchableOpacity, FlatList, Text, ImageBackground} from 'react-native';
import CardView from 'react-native-cardview';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Style from './style';

const FavouriteScreenView = (props) => {

  const {navigation,forceRerender,favouriteItemData,removeFromFavourite} = props;
    return (
       <View style={Style.mainContainer}>
         {
           favouriteItemData.length!==0?
             <>
             <FlatList
               style={Style.flatList}
               data={favouriteItemData}
               numColumns={3}
               renderItem={({ item }) =>
               {
                    return <TouchableOpacity
                      onPress={()=>navigation.navigate('DetailScreen',{
                        image: item.image,
                        title: item.title,
                        id:item.id,
                        description: item.description,
                        uploadDate: item.uploadDate,
                        rerender: forceRerender
                      })}
                      style={Style.touchable}>
                 <CardView
                     cardElevation={2}
                     cardMaxElevation={2}
                     style={Style.cardView}
                     cornerRadius={5}>
                       <ImageBackground
                         resizeMode='contain'
                         style={Style.image}
                         source={{uri: item.image}}>
                             <TouchableOpacity
                               onPress={()=>removeFromFavourite(item.id)}
                               style={Style.favourite}>
                               <MaterialIcons name={'favorite'} color={'#ff0000'} size={20}/>
                             </TouchableOpacity>
                       </ImageBackground>
                   </CardView>
                    </TouchableOpacity>
               }
               }
               keyExtractor={item => item.id}
             />
             </>
             :
             <View style={Style.noItemContainer}>
               <Text style={Style.noItemText}>No Item in Favourite</Text>
             </View>
         }
       </View>
    )
};

export default FavouriteScreenView;
