import React, {useState} from 'react';
import FavouriteScreenView from './view';
import connect from 'react-redux/lib/connect/connect';
import {favouriteItem} from '../../module/favouriteItem';

const FavouriteScreen = (props) => {

    const [state,setStates] = useState({
        rerender:false
    });

    const handleTextChange = (text,field) => {
        setStates(prev => ({
            ...prev,
            [field]: text,
        }))
    };

    const forceRerender = () => {
        const {navigation} = props;
        console.log(navigation);
        handleTextChange(!state.rerender,'rerender');
        navigation.state.params.rerender();
    };

    const removeFromFavourite = (id) => {
        let removedArray = props.favouriteItemData;
        for (let i=0;i<props.favouriteItemData.length;i++){
            if (props.favouriteItemData[i].id===id){
                removedArray.splice(i,1);
            }
        }
        props.dispatch(favouriteItem(removedArray)).then(()=>{
            forceRerender();
        });
    };

    return (
        <FavouriteScreenView
            navigation={props.navigation}
            favouriteItemData={props.favouriteItemData}
            removeFromFavourite={removeFromFavourite}
            forceRerender={forceRerender}
        />
    )
};

FavouriteScreen.navigationOptions = ({ navigation }) => ({
    title:'Favourite Item'
});

const mapStateToProps = state => ({
    favouriteItemData: state.favouriteItem.data
});

export default connect(mapStateToProps)(FavouriteScreen);
