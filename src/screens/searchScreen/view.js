import React from 'react';
import {View, TouchableOpacity, ActivityIndicator, FlatList, Text, TextInput, ImageBackground} from 'react-native';
import CardView from 'react-native-cardview';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Style from './style';

const StateScreenView = (props) => {

  const {data,navigation,onScroll,onSearch,forceRerender,favouriteItemData,totalItem,removeFromFavourite,markFavourite,handleTextChange,search,fetching} = props;
    return (
       <View style={Style.mainContainer}>
         <View style={Style.searchContainer}>
           <TextInput
             maxLength={25}
             placeholder={'Search'}
             style={Style.textInput}
             onChangeText={(text)=>handleTextChange(text,'search')}
             value={search}
           />
           <TouchableOpacity
             onPress={onSearch}
             style={Style.searchButton}>
             <Text style={{color:'#fff'}}>Search</Text>
           </TouchableOpacity>
         </View>
         {
           data.length!==0?
             <>
             <Text style={Style.totalItemCountText}>Total GIF Found: {totalItem}</Text>
             <FlatList
               style={Style.flatList}
               data={data}
               numColumns={3}
               onEndReachedThreshold={0.01}
               onEndReached={onScroll}
               renderItem={({ item }) =>
               {
                    return <TouchableOpacity
                      onPress={()=>navigation.navigate('DetailScreen',{
                        image: item.images.original.url,
                        title: item.title,
                        id:item.id,
                        description: item.user!==undefined?item.user.description:'No Description available',
                        uploadDate: item.import_datetime,
                        rerender: forceRerender
                      })}
                      style={Style.touchableCardView}>
                 <CardView
                     cardElevation={2}
                     cardMaxElevation={2}
                     style={Style.cardView}
                     cornerRadius={5}>
                       <ImageBackground
                         resizeMode='contain'
                         style={Style.image}
                         source={{uri: item.images.preview_gif.url!==undefined?item.images.preview_gif.url:item.images.original_still.url}}>
                         {
                           favouriteItemData.find(o => o.id === item.id)!==undefined?
                             <TouchableOpacity
                               onPress={()=>removeFromFavourite(item.id)}
                               style={Style.favouriteButton}>
                               <MaterialIcons name={'favorite'} color={'#ff0000'} size={20}/>
                             </TouchableOpacity>
                             :
                             <TouchableOpacity
                               onPress={()=>markFavourite(item.id,item.images.original.url,item.title,item.user!==undefined?item.user.description:'No Description available',item.import_datetime)}
                               style={Style.favouriteButton}>
                               <MaterialIcons name={'favorite-outline'} color={'#000'} size={20}/>
                             </TouchableOpacity>
                         }
                       </ImageBackground>
                     </CardView>
                    </TouchableOpacity>
               }
               }
               keyExtractor={item => item.id}
             />
             </>
             :
             <View style={Style.noItemView}>
               <Text style={Style.searchForGifText}>Search for any GIF</Text>
             </View>
         }
         {
           fetching===true&&
             <View style={Style.activityIndicatorContainer}>
               <ActivityIndicator size="large" color="#00ff00" />
             </View>
         }
       </View>
    )
};

export default StateScreenView;
