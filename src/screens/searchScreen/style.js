import {Dimensions, StyleSheet} from 'react-native';

const screenWidth = Dimensions.get('window').width;

const styles = StyleSheet.create({
  mainContainer: {
    flex:1
  },
  header:{
    width:50,
    justifyContent:'center',
    alignItems:'center',
    height:50
  },
  totalItemCountText:{
    fontSize: 14,
    marginLeft: 10
  },
  searchContainer:{
    justifyContent:'center',
    flexDirection:'row',
    width:'100%',
    height:45,
    padding:5
  },
  textInput:{
    borderWidth:1,
    paddingBottom:2,
    borderColor:'lightgrey',
    backgroundColor:'#fff',
    borderRadius:5,
    flex:1,
    elevation:10,
    marginHorizontal:5
  },
  searchButton:{
    backgroundColor:'red',
    borderColor:'lightgrey',
    paddingHorizontal:5,
    elevation:4,
    marginHorizontal:5,
    borderWidth:0.5,
    borderRadius:5,
    justifyContent:'center',
    alignItems:'center'
  },
  flatList:{
    flex:1
  },
  touchableCardView:{
    width:screenWidth/3,
    padding:5
  },
  cardView:{
    width:'100%',
    backgroundColor:'#000',
    flexDirection:'row',
    height:screenWidth/3,
    borderWidth:0.5
  },
  image:{
    flex:1,
    justifyContent:'flex-end',
    alignItems:'flex-end'
  },
  favouriteButton:{
    marginBottom:5,
    marginRight:5,
    justifyContent:'center',
    backgroundColor:'#fff',
    alignItems:'center',
    height:'25%',
    width:'25%',
    borderRadius:50
  },
  noItemView:{
    flex:1,
    justifyContent:'center',
    alignItems:'center'
  },
  searchForGifText:{
    fontWeight:'bold',
    color:'lightgrey',
    fontSize:22
  },
  activityIndicatorContainer:{
    position:'absolute',
    width:'100%',
    height:'100%',
    justifyContent:'center',
    alignItems:'center'
  }
});

export default styles;
