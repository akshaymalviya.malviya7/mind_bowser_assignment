import React, {useState,useEffect} from 'react';
import StateScreenView from './view';
import {fetchData} from '../../module/fetchData';
import {Keyboard, TouchableOpacity} from 'react-native';
import connect from 'react-redux/lib/connect/connect';
import {favouriteItem} from '../../module/favouriteItem';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Style from './style'

const StateScreen = (props) => {

    const [state,setStates] = useState({
        data:[],
        search:'',
        pageNumber:0,
        totalItem:0,
        rerender:false
    });

    const handleTextChange = (text,field) => {
        setStates(prev => ({
            ...prev,
            [field]: text,
        }))
    };

    const onSearch = () => {
        Keyboard.dismiss();
          handleTextChange([],'data');
          handleTextChange(0,'pageNumber');
          handleTextChange(0,'totalItem');
          const payload = {
              query: state.search,
              pageNumber: 0
          };
          props.dispatch(fetchData(payload)).then((response)=>{
              handleTextChange(response.data,'data');
              handleTextChange(response.pagination.total_count,'totalItem')
          }).catch((error)=>{
              console.log(error);
          });
    };

    const onScroll = () => {
        if ((state.pageNumber+1)*50<state.totalItem){
            const payload = {
                query: state.search,
                pageNumber: (state.pageNumber+1)*50
            };
            props.dispatch(fetchData(payload)).then((response)=>{
                handleTextChange(state.pageNumber+1,'pageNumber');
                handleTextChange(state.data.concat(response.data),'data');
            }).catch((error)=>{
                console.log(error);
            });
        }
    };

    const markFavourite = (id,image,title,description,uploadDate) => {
        if(props.favouriteItemData.length<5){
                const payload = [{
                    image: image,
                    title:title,
                    id: id,
                    description: description,
                    uploadDate: uploadDate
                }];
                props.dispatch(favouriteItem(payload.concat(props.favouriteItemData)));
        }else{
            console.log("5 hogaye");
        }
    };

    const forceRerender = () => {
        handleTextChange(!state.rerender,'rerender')
    };

    useEffect(()=>{
        navigation.setParams({
            rerender: forceRerender
        })
    },[]);

    const removeFromFavourite = (id) => {
        let removedArray = props.favouriteItemData;
        for (let i=0;i<props.favouriteItemData.length;i++){
            if (props.favouriteItemData[i].id===id){
                removedArray.splice(i,1);
            }
        }
        props.dispatch(favouriteItem(removedArray)).then(()=>{
            forceRerender();
        });
    };

    const {navigation,fetching,favouriteItemData} = props;
    const {data,search,totalItem} = state;

    return (
        <StateScreenView
            navigation={navigation}
            data={data}
            handleTextChange={handleTextChange}
            search={search}
            fetching={fetching}
            onSearch={onSearch}
            totalItem={totalItem}
            onScroll={onScroll}
            favouriteItemData={favouriteItemData}
            markFavourite={markFavourite}
            removeFromFavourite={removeFromFavourite}
            forceRerender={forceRerender}
        />
    )
};

StateScreen.navigationOptions = ({ navigation }) => ({
    title:'Search Screen',
    headerRight: (<TouchableOpacity
      onPress={()=>navigation.navigate('FavouriteScreen',{
          rerender:navigation.getParam('rerender')
      })}
      style={Style.header}>
        <MaterialIcons name={'favorite-outline'} color={'#000'} size={40}/>
    </TouchableOpacity>)
});

const mapStateToProps = state => ({
    fetching: state.fetchData.fetching,
    favouriteItemData: state.favouriteItem.data
});

export default connect(mapStateToProps)(StateScreen);
