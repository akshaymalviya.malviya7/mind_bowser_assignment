import React from 'react';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import MainScreen from '../screens/mainScreen';
import StateScreen from '../screens/searchScreen';
import DetailScreen from '../screens/detailScreen';
import FavouriteScreen from '../screens/favouriteScreen';

const AppNavigator = createStackNavigator({
  MainScreen,
  StateScreen,
  DetailScreen,
  FavouriteScreen
},{
    initialRouteName:'MainScreen'
});

export default createAppContainer(AppNavigator);
