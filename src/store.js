import  AsyncStorage  from '@react-native-community/async-storage';
import { createStore, applyMiddleware, combineReducers } from 'redux';
import { persistStore, persistReducer } from 'redux-persist';
import thunkMiddleware from 'redux-thunk';
import fetchData from './module/fetchData';
import favouriteItem from './module/favouriteItem';

const middleware = [thunkMiddleware];
const persistConfig = {
    key: 'root',
    storage: AsyncStorage,
    whitelist: [
        'favouriteItem',
    ]
};

//Combine Reducer
const AppReducer = combineReducers({
  fetchData,
  favouriteItem
});

const persistReducers = persistReducer(persistConfig, AppReducer);


export const store = createStore(persistReducers, applyMiddleware(...middleware));
export const persistor = persistStore(store);
