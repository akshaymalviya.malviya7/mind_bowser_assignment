// Action type for FavouriteItem
export const FAVOURITE = 'FAVOURITE';

export const favouriteItem = (favouriteItem) => {
    return (dispatch) => new Promise((resolve) => {
        dispatch({
            favouriteItem,
            type: FAVOURITE
        });
        resolve(favouriteItem);
    });
};

const reducer = (state = {
    data: []
}, action) => {
    switch (action.type) {
        case FAVOURITE: {
            return {
                ...state,
                data: action.favouriteItem,
            };
        }
        default: {
            return state;
        }
    }
};

export default reducer;

