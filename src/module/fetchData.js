import Axios from 'axios';

// Action type for fetching data
export const DATA_REQUEST = 'DATA_REQUEST';
export const DATA_SUCCESS = 'DATA_SUCCESS';
export const DATA_FAIL = 'DATA_FAIL';

export const fetchData = (payload) => {
    return (dispatch) => new Promise((resolve, reject) => {
        dispatch({
            type: DATA_REQUEST
        });
        Axios.get(`https://api.giphy.com/v1/gifs/search?api_key=TxF6SgPaLE98MCn7Y0fMLThwBIrsoGtB&q=${payload.query}&limit=50&offset=${payload.pageNumber}&rating=g&lang=en`).then((response) => {
            dispatch({
                data: response.data,
                type: DATA_SUCCESS,
            });
            resolve(response.data);
            console.log(response.data);
        }).catch((error) => {
            dispatch({
                type: DATA_FAIL
            });
            reject(error);
        });
    });
};

const reducer = (state = {
    fetching: false,
    data: []
}, action) => {
    switch (action.type) {
        case DATA_REQUEST: {
            return {
                ...state,
                fetching: true,
            };
        }
        case DATA_SUCCESS: {
            return {
                ...state,
                fetching: false,
                data: action.data,
            };
        }
        case DATA_FAIL: {
            return {
                ...state,
                fetching: false,
                error: action.error,
            };
        }
        default: {
            return state;
        }
    }
};
export default reducer;
