/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import AppNavigator from './src/navigators/root';
import { Provider} from 'react-redux';
import {store,persistor} from './src/store';
import { PersistGate } from 'redux-persist/lib/integration/react';

const App: () => React$Node = () => {
  return (
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <AppNavigator/>
        </PersistGate>
      </Provider>
  );
};

export default App;
